//
//  ResdetailsVC.h
//  FortuneCookie
//
//  Created by Aditi Manna on 05/10/15.
//  Copyright © 2015 Limtex Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ResList.h"
@interface ResdetailsVC : UIViewController
- (IBAction)btnSubmitAction:(id)sender;
@property (nonatomic,retain) NSString *resId;
@property (nonatomic,retain) ResList *rest;

@property (nonatomic,retain) IBOutlet UILabel *nameLabel, *addressLabel, *phoneLabel, *descLabel;

@end
