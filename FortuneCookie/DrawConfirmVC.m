//
//  DrawConfirmVC.m
//  FortuneCookie
//
//  Created by Aditi Manna on 07/10/15.
//  Copyright © 2015 Limtex Infotech. All rights reserved.
//

#import "DrawConfirmVC.h"
#import "SSTabBarController.h"
#import "AppDelegate.h"
#import "WebserviceManager.h"
@interface DrawConfirmVC ()

{
    SSTabBarController *st;
    AppDelegate *appdel;
}
- (IBAction)btnBackAction:(UIButton *)sender;



- (IBAction)btnViewEntryAction:(id)sender;
@end

@implementation DrawConfirmVC

- (void)viewDidLoad {
    [super viewDidLoad];
    appdel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    self.lblResName.text = self.rest1.strResName;
    self.lblResAddr.text = self.rest1.strAddr;
    self.lblPhn.text = self.rest1.strphn;
    
    
    NSString *str = self.strDrawDt;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    NSDate *date = [dateFormatter dateFromString: str];
    
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
    NSString *convertedString = [dateFormatter stringFromDate:date];
    self.lblDrawDt.text= convertedString;
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnBackAction:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)btnViewEntryAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        [appdel.customTabBarVC processCompleted:2];
      //  st = [[SSTabBarController alloc] initWithNibName:@"SSTabBarController" bundle:nil];
        
        //st proc
    }];
    
}
@end
