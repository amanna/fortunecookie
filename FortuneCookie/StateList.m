//
//  StateList.m
//  FortuneCookie
//
//  Created by Aditi Manna on 09/11/15.
//  Copyright © 2015 Limtex Infotech. All rights reserved.
//

#import "StateList.h"

@implementation StateList
- (id)initWithDict:(NSDictionary *)dict {
    if (self = [super init]) {
        self.strStateId = [dict objectForKey:@"id"];
        self.strStateName = [dict objectForKey:@"state_name"];
    }
    return self;
}

@end
