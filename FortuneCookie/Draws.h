//
//  Draws.h
//  FortuneCookie
//
//  Created by SampritaRoy on 20/11/15.
//  Copyright © 2015 Limtex Infotech. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Draws : NSObject
@property(nonatomic)NSString *strDrawDt;
@property(nonatomic)NSString *strDrawid;
@property(nonatomic)NSString *strResId;
@property(nonatomic)NSString *strResName;
@property(nonatomic)NSString *strtotal;
@property(nonatomic)NSString *strStatus;
- (id)initWithDict:(NSDictionary *)dict;
@end
