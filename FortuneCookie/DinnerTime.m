//
//  DinnerTime.m
//  FortuneCookie
//
//  Created by Aditi Manna on 13/11/15.
//  Copyright © 2015 Limtex Infotech. All rights reserved.
//

#import "DinnerTime.h"

@implementation DinnerTime
- (id)initWithDict:(NSDictionary *)dict{
     if (self = [super init]) {
     self.strDinnerDay = [dict objectForKey:@"day"];
     self.strDinnerTime = [dict objectForKey:@"dinner_time"];
     }
    return self;
}
@end
