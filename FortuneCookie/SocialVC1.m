//
//  SocialVC1.m
//  FortuneCookie
//
//  Created by Aditi Manna on 27/11/15.
//  Copyright © 2015 Limtex Infotech. All rights reserved.
//

#import "SocialVC1.h"

@interface SocialVC1 ()

@end

@implementation SocialVC1

- (void)viewDidLoad {
    [super viewDidLoad];
    // Build the url and loadRequest
    
    //NSString *strFbTw = [NSString stringWithFormat:@"https://%@",self.strUrl];
    // self.strUrl = @"https://www.google.co.in";
    [self.myweb loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.strUrl]]];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)btnCloseAction:(UIButton *)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
