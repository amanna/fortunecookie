//
//  Draws.m
//  FortuneCookie
//
//  Created by SampritaRoy on 20/11/15.
//  Copyright © 2015 Limtex Infotech. All rights reserved.
//

#import "Draws.h"

@implementation Draws
- (id)initWithDict:(NSDictionary *)dict{
   if (self = [super init]) {
        self.strDrawDt = [dict objectForKey:@"draw_date"];
        self.strDrawid = [NSString stringWithFormat:@"%@",[dict objectForKey:@"id"]];
        self.strResId = [dict objectForKey:@"restaurant_id"];
        self.strResName = [dict objectForKey:@"restaurant_name"];
        self.strtotal = [dict objectForKey:@"total_participant"];
       self.strStatus = [dict objectForKey:@"status"];
       
    }
    return self;
}
@end
