//
//  MyAnnotation.m
//  FortuneCookie
//
//  Created by Aditi Manna on 12/11/15.
//  Copyright © 2015 Limtex Infotech. All rights reserved.
//

#import "MyAnnotation.h"

@implementation MyAnnotation
@synthesize coordinate;

@synthesize title;
@synthesize subtitle;
@synthesize pointid;


- (id)initWithCoordinates:(CLLocationCoordinate2D)location placeName:placeName description:description point:(int)point {
    self = [super init];
    if (self != nil) {
        coordinate = location;
        title = placeName;
        subtitle = description;
        pointid=point;
    }
    return self;
}


@end
