//
//  MyDraws.m
//  FortuneCookie
//
//  Created by Aditi Manna on 06/10/15.
//  Copyright © 2015 Limtex Infotech. All rights reserved.
//

#import "MyDraws.h"
#import "SearchVCCell.h"
#import "MyDrawCell.h"
#import "MyDrawLiveVC.h"
#import "FCWinning.h"
#import "WebserviceManager.h"
#import "Draws.h"
#import "SelectState.h"
#import "LoginVC.h"
#import "AppDelegate.h"
@interface MyDraws (){
    AppDelegate *appdel;
}
@property (weak, nonatomic) IBOutlet UITableView *tblview;
@property(nonatomic)NSMutableArray *arrDraw;
- (IBAction)btnSettingsAction:(UIButton *)sender;

@end

@implementation MyDraws
- (void)viewDidAppear:(BOOL)animated{
    appdel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    self.arrDraw = [[NSMutableArray alloc]init];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    // getting an NSString
    NSString *strUser = [prefs stringForKey:@"uid"];
    self.tblview.hidden = YES;
    [WebserviceManager callMyDraw:strUser OnCompletion:^(id object, NSError *error) {
        if(object){
            self.tblview.hidden = NO;
            self.arrDraw = object;
            [self.tblview reloadData];
        }else{
            
        }
        
    }];

}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.arrDraw.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   // static NSString *simpleTableIdentifier = @"Cell";
    static NSString *simpleTableIdentifier = @"Cell";
    
    MyDrawCell *cell=(MyDrawCell *)[self.tblview dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    NSInteger i = indexPath.row;
    Draws *draw = [self.arrDraw objectAtIndex:indexPath.row];
    cell.lblResName.text = draw.strResName;
    
    NSString *str = draw.strDrawDt;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    NSDate *date = [dateFormatter dateFromString: str];
    
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
    NSString *convertedString = [dateFormatter stringFromDate:date];
    cell.lblDrawDt.text = [NSString stringWithFormat:@"Draw Date - %@",convertedString];
    cell.lblChance.text = [NSString stringWithFormat:@"1 in %@ chance",draw.strtotal];
    NSString *strStatus = [NSString stringWithFormat:@"Status:%@",draw.strStatus];
     cell.lblStatus.text = strStatus;
    
//    if(i % 2 ==0){
//       
//        
//    }else{
//        cell.lblStatus.text = @"Status:NO Win";
//    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
      NSInteger i = indexPath.row;
     if(i % 2 ==0){
         UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"MyDrawLive"
                                                              bundle:nil];
         
        Draws *draw = [self.arrDraw objectAtIndex:indexPath.row];
         
    MyDrawLiveVC *add =
    [storyboard instantiateViewControllerWithIdentifier:@"MyDrawLiveVC"];
         add.strDrawDt = draw.strDrawDt;
         add.strResId = draw.strResId;
         add.strResName = draw.strResName;
         
    [self presentViewController:add
                       animated:YES
                     completion:nil];
     }else{
         UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main"
                                                              bundle:nil];
         SelectState *add =
         [storyboard instantiateViewControllerWithIdentifier:@"SelectState"];
         add.strType= @"settings";
         
         [self presentViewController:add
                            animated:YES
                          completion:nil];

     }
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnSettingsAction:(UIButton *)sender {
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main"
                                                         bundle:nil];
    SelectState *add =
    [storyboard instantiateViewControllerWithIdentifier:@"SelectState"];
    add.strType= @"settings";
    
    [self presentViewController:add
                       animated:YES
                     completion:nil];


}
- (IBAction)btnLogoutAction:(UIButton *)sender {
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Logout" message:@"Are you want to sure log out?"
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:@"Cancel",nil];
    
    [alert show];
    
    //[self performSegueWithIdentifier:@"segueLogin" sender:self];
   
    
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex==0){
         [appdel removeTab];
    }
    
}

@end
