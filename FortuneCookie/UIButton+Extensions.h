//
//  UIButton+Extensions.h
//  FortuneCookie
//
//  Created by Aditi Manna on 15/10/15.
//  Copyright © 2015 Limtex Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (Extensions)
@property(nonatomic, assign) UIEdgeInsets hitTestEdgeInsets;
@end
