//
//  AppDelegate.h
//  FortuneCookie
//
//  Created by Aditi Manna on 02/10/15.
//  Copyright © 2015 Limtex Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MapVC.h"
#import "SearchVC.h"
#import "MyDraws.h"
#import "SSTabBarController.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    
}

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic) SSTabBarController *customTabBarVC;
- (void)createTab;
- (void)removeTab;
@end

