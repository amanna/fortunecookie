//
//  MapVC.h
//  FortuneCookie
//
//  Created by Aditi Manna on 05/10/15.
//  Copyright © 2015 Limtex Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
@interface MapVC : UIViewController
@property (nonatomic,retain)IBOutlet MKMapView *mapView;
@end
