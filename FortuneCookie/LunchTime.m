//
//  LunchTime.m
//  FortuneCookie
//
//  Created by Aditi Manna on 13/11/15.
//  Copyright © 2015 Limtex Infotech. All rights reserved.
//

#import "LunchTime.h"

@implementation LunchTime
- (id)initWithDict:(NSDictionary *)dict{
    if (self = [super init]) {
        self.strLunchDay = [dict objectForKey:@"day"];
        self.strLunchTime = [dict objectForKey:@"lunch_time"];
    }
    return self;
}
@end
