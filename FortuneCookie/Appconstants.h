//
//  Appconstants.h
//  VideoShare
//
//  Created by Aditi Manna on 11/06/15.
//  Copyright (c) 2015 Limtex Infotech. All rights reserved.
//

#ifndef VideoShare_Appconstants_h
#define VideoShare_Appconstants_h

#define kBaseURL @"http://dev.businessprodemo.com/qratetv/php/"
#define kBaseURL1  @"http://qrate.tv/"
#define kFbLoginUrl @"http://dev1.businessprodemo.com/FortuneCookie/php/index.php?r=api/FacebookLogin"
#define kstatelist @"http://dev1.businessprodemo.com/FortuneCookie/php/index.php?r=api/StateList"
#define kreslist @"http://dev1.businessprodemo.com/FortuneCookie/php/index.php?r=api/RestaurantList"
#define kresdetails @"http://dev1.businessprodemo.com/FortuneCookie/php/index.php?r=api/GetRestaurantDetails"
#define kUniqueCode @"http://dev1.businessprodemo.com/FortuneCookie/php/index.php?r=api/DrawSubmit"
#define kMyDraw @"http://dev1.businessprodemo.com/FortuneCookie/php/index.php?r=api/MyDraws"
#define kMyDrawDetails @"http://dev1.businessprodemo.com/FortuneCookie/php/index.php?r=api/DrawDetails"
#define kNoInternet @"No Internet! Please connect to a wifi network or activate cellular data internet."
#endif
