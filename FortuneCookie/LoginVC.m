//
//  LoginVC.m
//  FortuneCookie
//
//  Created by Aditi Manna on 05/10/15.
//  Copyright © 2015 Limtex Infotech. All rights reserved.
//

#import "LoginVC.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "AppDelegate.h"
#import "WebserviceManager.h"
#import "UtilityManager.h"
@interface LoginVC (){
    AppDelegate *appdel;
}

@end

@implementation LoginVC
- (IBAction)btnSkipAction:(id)sender {
//     appdel = (AppDelegate *)[UIApplication sharedApplication].delegate;
     [appdel createTab];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    appdel = (AppDelegate *)[UIApplication sharedApplication].delegate;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)btnFbAction:(UIButton *)sender {
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithReadPermissions:@[@"email"] handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        if (error) {
            // Process error
        } else if (result.isCancelled) {
            // Handle cancellations
            NSLog(@"cancel");
        } else {
            // If you ask for multiple permissions at once, you
            // should check if specific permissions missing
            NSLog(@"cancel123");
            if ([result.grantedPermissions containsObject:@"email"]) {
                // Do work
                if ([FBSDKAccessToken currentAccessToken]) {
                    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:nil]
                     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                         if (!error) {
                             // [self performSegueWithIdentifier:@"segueMenu" sender:self];
                             NSDictionary *dict = (NSDictionary*)result;
                             NSLog(@"fetched user:%@", result);
                             //Login Webservice
                             NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];  //load NSUserDefaults
                             NSString *stateId = [prefs valueForKey:@"state"];
                             NSString *fbId = [result valueForKey:@"id"];
                             NSString *fullname = [result valueForKey:@"name"];
                             NSArray *listItems = [fullname componentsSeparatedByString:@" "];
                             NSString *fname = [listItems objectAtIndex:0];
                             NSString *username = [listItems objectAtIndex:0];
                             NSString *email = [NSString stringWithFormat:@"%@@facebook.com",fname];
                             //email = @"aditi.manna@businessprodesigns.com";
                             NSString *lname=[fullname substringFromIndex:fullname.length-fname.length];
                             
                             
                             NSString *fbProfileurl=[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large&return_ssl_resources=1", fbId];
                             NSDictionary *dictFb = [NSDictionary dictionaryWithObjectsAndKeys:username,@"username",email,@"email" ,stateId,@"state",fbProfileurl,@"fbimage",fbId,@"fbId",fname,@"fname",lname,@"lname",nil];

                             
                            [WebserviceManager callFBLogin:dictFb OnCompletion:^(id object, NSError *error) {
                                if(object){
                                    NSString *str = object;
                                    if([str isEqualToString:@"1"]){
//                                        [self performSegueWithIdentifier:@"segueSearch" sender:self];
                                        [appdel createTab];
                                    }
                                    
                                }else{
                                    [UtilityManager showAlertWithMessage:@"Some Problem occurs!!Please try again later"];
                                }
                            }];
                          
                         }
                     }];
                }
                
            }
        }
    }];

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
