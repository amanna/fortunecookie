//
//  StateList.h
//  FortuneCookie
//
//  Created by Aditi Manna on 09/11/15.
//  Copyright © 2015 Limtex Infotech. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StateList : NSObject
@property(nonatomic)NSString *strStateId;
@property(nonatomic)NSString *strStateName;
- (id)initWithDict:(NSDictionary *)dict;

@end
