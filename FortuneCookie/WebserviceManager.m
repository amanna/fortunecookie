//
//  WebserviceManager.m
//  VideoShare
//
//  Created by Aditi Manna on 11/06/15.
//  Copyright (c) 2015 Limtex Infotech. All rights reserved.
//

#import "WebserviceManager.h"
#import "SSRestManager.h"
#import "Appconstants.h"
#import "AppDelegate.h"
#import "StateList.h"
#import "ResList.h"
#import "Draws.h"
#import "Participant.h"
//{"status":1,"error":"","result":[]}

@implementation WebserviceManager

+ (void)callFBLogin:(NSDictionary*)dictFb OnCompletion:(FetchCompletionHandler)handler{
    SSRestManager *restManager = [[SSRestManager alloc] init];
    NSString *menuUrl;
    menuUrl = [NSString stringWithFormat:@"%@",kFbLoginUrl];
    
//    NSDictionary *dictFb = [NSDictionary dictionaryWithObjectsAndKeys:username,@"username",email,@"email" ,stateId,@"state",fbId,@"fbId",fname,@"fname",lname,@"lname",nil];
//    
//
//    User[username]
//    User[email]
//    User[state_id]
//    Profile[facebook_id]
//    Profile[firstname]
//    Profile[lastname]fbimage
    NSString *strQuery = [NSString stringWithFormat:@"User[username]=%@&User[email]=%@&User[state_id]=%@&User[image]=%@&Profile[facebook_id]=%@&Profile[firstname]=%@&Profile[lastname]=%@",[dictFb valueForKey:@"username"], [dictFb valueForKey:@"email"],[dictFb valueForKey:@"state"],[dictFb valueForKey:@"fbimage"],[dictFb valueForKey:@"fbId"],[dictFb valueForKey:@"fname"],[dictFb valueForKey:@"lname"]];
    [restManager getJsonResponseFromBaseUrl:menuUrl query:strQuery onCompletion:^(NSDictionary *json) {
        if (json) {
            NSDictionary *dictStat = [json valueForKey:@"status"];
            NSInteger status = [[dictStat valueForKey:@"status"]integerValue];
            NSString *strStatus = [NSString stringWithFormat:@"%ld",(long)status];
            if([strStatus isEqualToString:@"1"]){
                
                NSDictionary *dictRes = [json valueForKey:@"result"];
                NSDictionary *dictUser = [dictRes valueForKey:@"user"];
               
                NSInteger status = [[dictUser valueForKey:@"status"]integerValue];
                
                if(status == 1){
                     NSInteger uid = [[dictUser valueForKey:@"id"]integerValue];
                    NSString *strUid = [NSString stringWithFormat:@"%ld",(long)uid];
                    NSString *strUName = [dictUser valueForKey:@"username"];
                     NSString *strImage = [dictUser valueForKey:@"image"];
                    NSString *strState = [dictFb valueForKey:@"state"];
                    [[NSUserDefaults standardUserDefaults] setObject:strUid forKey:@"uid"];
                    [[NSUserDefaults standardUserDefaults] setObject:strUName forKey:@"username"];
                    [[NSUserDefaults standardUserDefaults] setObject:strImage forKey:@"fbimage"];
                     [[NSUserDefaults standardUserDefaults] setObject:strState forKey:@"state"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                }
            }
            handler(strStatus,nil);
        }
    } onError:^(NSError *error) {
        if (error) {
            handler(nil,error);
        }
    }];
    
}
+ (void)callStateList:(FetchCompletionHandler)handler{
    SSRestManager *restManager = [[SSRestManager alloc] init];
    NSString *menuUrl;
    menuUrl = [NSString stringWithFormat:@"%@",kstatelist];
       [restManager getJsonResponseFromBaseUrl:menuUrl query:nil onCompletion:^(NSDictionary *json) {
        if (json) {
            NSDictionary *dictStat = [json valueForKey:@"status"];
            NSInteger status = [[dictStat valueForKey:@"status"]integerValue];
            NSString *strStatus = [NSString stringWithFormat:@"%ld",(long)status];
            if([strStatus isEqualToString:@"1"]){
                 NSDictionary *dictRes = [json valueForKey:@"result"];
                 NSArray *arrState = [dictRes valueForKey:@"state"];
                
                NSMutableArray *arrStateMut = [[NSMutableArray alloc]init];
                 [arrState enumerateObjectsUsingBlock:^(NSDictionary *dic, NSUInteger idx, BOOL * _Nonnull stop) {
                      StateList *vid= [[StateList alloc]initWithDict:dic];
                     [arrStateMut addObject:vid];
                     
                 }];
                
                handler(arrStateMut,nil);
            }
            
        }
    } onError:^(NSError *error) {
        if (error) {
            handler(nil,error);
        }
    }];

}
+ (void)callResList:(NSString*)strSearch  OnCompletion:(FetchCompletionHandler)handler{
    SSRestManager *restManager = [[SSRestManager alloc] init];
    NSString *menuUrl;
    menuUrl = [NSString stringWithFormat:@"%@",kreslist];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];  //load NSUserDefaults
    NSString *stateId = [prefs valueForKey:@"state"];

    NSString *strQuery = [NSString stringWithFormat:@"state_id=%@&search_string=%@",stateId,strSearch];
    
    [restManager getJsonResponseFromBaseUrl:menuUrl query:strQuery onCompletion:^(NSDictionary *json) {
        if (json) {
            NSDictionary *dictStat = [json valueForKey:@"status"];
            NSInteger status = [[dictStat valueForKey:@"status"]integerValue];
            NSString *strStatus = [NSString stringWithFormat:@"%ld",(long)status];
            NSMutableArray *arrStateMut = [[NSMutableArray alloc]init];
            if([strStatus isEqualToString:@"1"]){
                NSDictionary *dictRes = [json valueForKey:@"result"];
                NSArray *arrState = [dictRes valueForKey:@"restaurant"];
                
//                NSMutableArray *arrStateMut = [[NSMutableArray alloc]init];
                [arrState enumerateObjectsUsingBlock:^(NSDictionary *dic, NSUInteger idx, BOOL * _Nonnull stop) {
                    ResList *vid= [[ResList alloc]initWithDict:dic withResList:true];
                    [arrStateMut addObject:vid];
                    
                }];
                
                handler(arrStateMut,nil);
            }
            else
            {
                NSLog(@"No data");
                handler(arrStateMut,nil);
            }
            
        }
    } onError:^(NSError *error) {
        if (error) {
            handler(nil,error);
        }
    }];

}
+ (void)callResDetails:(NSString*)strRes OnCompletion:(FetchCompletionHandler)handler{
    SSRestManager *restManager = [[SSRestManager alloc] init];
    NSString *menuUrl;
    menuUrl = [NSString stringWithFormat:@"%@",kresdetails];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];  //load NSUserDefaults
    NSString *stateId = [prefs valueForKey:@"state"];
    
    NSString *strQuery = [NSString stringWithFormat:@"restaurant_id=%@",strRes];
  [restManager getJsonResponseFromBaseUrl:menuUrl query:strQuery onCompletion:^(NSDictionary *json) {
        if (json) {
            NSDictionary *dictStat = [json valueForKey:@"status"];
            NSInteger status = [[dictStat valueForKey:@"status"]integerValue];
            NSString *strStatus = [NSString stringWithFormat:@"%ld",(long)status];
            if([strStatus isEqualToString:@"1"]){
                NSDictionary *dictRes = [json valueForKey:@"result"];
                 NSDictionary *dictResturent = [dictRes valueForKey:@"restaurant"];
                NSMutableArray *arrStateMut = [[NSMutableArray alloc]init];
                 ResList *vid= [[ResList alloc]initWithDict:dictResturent withResList:NO];
                
            
                
                handler(vid,nil);
            }
            
        }
    } onError:^(NSError *error) {
        if (error) {
            handler(nil,error);
        }
    }];

}
+ (void)callSubmitCode:(NSString*)strRes withUser:(NSString*)strUser withUniqueCode:(NSString*)strUnique withOTP:(NSString*)strOtp OnCompletion:(FetchCompletionHandler1)handler{
    SSRestManager *restManager = [[SSRestManager alloc] init];
    NSString *menuUrl;
    menuUrl = [NSString stringWithFormat:@"%@",kUniqueCode];
    NSString *strQuery = [NSString stringWithFormat:@"restaurant_id=%@&user_id=%@&unique_code=%@",strRes,strUser,strUnique];
    [restManager getJsonResponseFromBaseUrl:menuUrl query:strQuery onCompletion:^(NSDictionary *json) {
        if (json) {
            NSDictionary *dictStat = [json valueForKey:@"status"];
            NSInteger status = [[dictStat valueForKey:@"status"]integerValue];
            NSString *strError = [dictStat valueForKey:@"errorMsg"];
            NSString *strStatus = [NSString stringWithFormat:@"%ld",(long)status];
            if([strStatus isEqualToString:@"1"]){
                NSDictionary *dictres = [[json valueForKey:@"result"]valueForKey:@"drawDetails"];
                NSString *strDrawDt = [dictres valueForKey:@"draw_date"];
                handler(strStatus,strDrawDt,nil);
            }else{
                handler(strStatus,strError,nil);
            }
            
        }
    } onError:^(NSError *error) {
        if (error) {
            handler(nil,nil,error);
        }
    }];

}
+ (void)callMyDraw:(NSString*)strUser OnCompletion:(FetchCompletionHandler)handler{
    SSRestManager *restManager = [[SSRestManager alloc] init];
    NSString *menuUrl;
    menuUrl = [NSString stringWithFormat:@"%@",kMyDraw];
    
    NSString *strQuery = [NSString stringWithFormat:@"user_id=%@",strUser];
    [restManager getJsonResponseFromBaseUrl:menuUrl query:strQuery onCompletion:^(NSDictionary *json) {
        if (json) {
            NSDictionary *dictStat = [json valueForKey:@"status"];
            NSInteger status = [[dictStat valueForKey:@"status"]integerValue];
            NSString *strStatus = [NSString stringWithFormat:@"%ld",(long)status];
            if([strStatus isEqualToString:@"1"]){
                NSDictionary *dictRes = [json valueForKey:@"result"];
                NSArray *arrResturent = [dictRes valueForKey:@"drawDetails"];
                
                NSMutableArray *arrStateMut = [[NSMutableArray alloc]init];
                
                
                [arrResturent enumerateObjectsUsingBlock:^(NSDictionary *dic, NSUInteger idx, BOOL * _Nonnull stop) {
                    Draws *vid= [[Draws alloc]initWithDict:dic];
                    [arrStateMut addObject:vid];
                    
                }];
               
                handler(arrStateMut,nil);
            }
            
        }
    } onError:^(NSError *error) {
        if (error) {
            handler(nil,error);
        }
    }];

}
+ (void)callDrawDetails:(NSString*)strDrawDate andUserId:(NSString*)strUser andResId:(NSString*)strRes OnCompletion:(FetchCompletionHandler)handler{
    SSRestManager *restManager = [[SSRestManager alloc] init];
    NSString *menuUrl;
    menuUrl = [NSString stringWithFormat:@"%@",kMyDrawDetails];
    
   
    NSString *strQuery = [NSString stringWithFormat:@"user_id=%@&draw_date=%@&restaurant_id=%@",strUser,strDrawDate,strRes];
    [restManager getJsonResponseFromBaseUrl:menuUrl query:strQuery onCompletion:^(NSDictionary *json) {
        if (json) {
            NSDictionary *dictStat = [json valueForKey:@"status"];
            NSInteger status = [[dictStat valueForKey:@"status"]integerValue];
            NSString *strStatus = [NSString stringWithFormat:@"%ld",(long)status];
            if([strStatus isEqualToString:@"1"]){
                NSDictionary *dictRes = [json valueForKey:@"result"];
                NSArray *arrResturent = [dictRes valueForKey:@"participants"];
                
                NSMutableArray *arrStateMut = [[NSMutableArray alloc]init];
                
                
                [arrResturent enumerateObjectsUsingBlock:^(NSDictionary *dic, NSUInteger idx, BOOL * _Nonnull stop) {
                    Participant *part= [[Participant alloc]initWithDict:dic];
                    [arrStateMut addObject:part];
                    
                }];
                
                handler(arrStateMut,nil);
            }
            
        }
    } onError:^(NSError *error) {
        if (error) {
            handler(nil,error);
        }
    }];

}
//+(void)fetchRecentVideoOnCompletion:(NSString*)strVideoType withPage:(NSInteger)pageNo OnCompletion:(FetchCompletionHandler)handler{
//    SSRestManager *restManager = [[SSRestManager alloc] init];
//    NSString *menuUrl;
//    if([strVideoType isEqualToString:@"recent"]){
//        menuUrl = [NSString stringWithFormat:@"%@%@",kBaseURL,@"webservice_recentlycreated"];
//    }else if ([strVideoType isEqualToString:@"popular"]){
//        menuUrl = [NSString stringWithFormat:@"%@%@",kBaseURL,@"webservice_mostpopularvideos"];
//    }else{
//        menuUrl = [NSString stringWithFormat:@"%@%@",kBaseURL,@"webservice_highestratedviedeos"];
//    }
//    NSString *strPage = [NSString stringWithFormat:@"pagenumber=%d",pageNo];
//   [restManager getJsonResponseFromBaseUrl:menuUrl query:strPage onCompletion:^(NSDictionary *json) {
//        if (json) {
//            NSMutableArray *vidFullList = [[NSMutableArray alloc] init];
//            NSArray *vidList = [json valueForKey:@"result"];
//                if(vidList.count > 0){
//                [vidList enumerateObjectsUsingBlock:^(NSDictionary *vidDict, NSUInteger idx, BOOL *stop) {
//                    Videos *vid= [[Videos alloc]initWithDict:vidDict];
//                    [vidFullList addObject:vid];
//                    
//                }];
//            }
//            
//            handler (vidFullList,nil);
//        }
//    } onError:^(NSError *error) {
//        if (error) {
//            handler(nil,error);
//        }
//    }];
//}
//+ (void)fetchVideoDataWithLink:(NSString*)strVideoType andQueueId:(NSString *)strQueueId OnCompletion:(FetchCompletionHandler)handler{
//    SSRestManager *restManager = [[SSRestManager alloc] init];
//    NSString *menuUrl;
//    if([strVideoType isEqualToString:@"recent"]){
//        menuUrl = [NSString stringWithFormat:@"%@%@",kBaseURL,@"webservice_recently_created_video_details"];
//    }else if ([strVideoType isEqualToString:@"popular"]){
//        menuUrl = [NSString stringWithFormat:@"%@%@",kBaseURL,@"webservice_mostpopular_video_details"];
//    }else{
//        menuUrl = [NSString stringWithFormat:@"%@%@",kBaseURL,@"webservice_highestrated_video_details"];
//    }
//    strQueueId = [NSString stringWithFormat:@"queueid=%@",strQueueId];
//    [restManager getJsonResponseFromBaseUrl:menuUrl query:strQueueId onCompletion:^(NSDictionary *json) {
//        if (json) {
//            NSMutableArray *vidFullList = [[NSMutableArray alloc] init];
//            NSInteger status = [[json valueForKey:@"status"]integerValue];
//            if(status==1){
//                NSArray *vidList = [json valueForKey:@"result"];
//                [vidList enumerateObjectsUsingBlock:^(NSDictionary *vidDict, NSUInteger idx, BOOL *stop) {
//                    VideoDetails *vid= [[VideoDetails alloc]initWithDict:vidDict];
//                    [vidFullList addObject:vid];
//                    
//                }];
//                
//                handler(vidFullList,nil);
//            }
//        }
//    } onError:^(NSError *error) {
//        if (error) {
//            handler(nil,error);
//        }
//    }];
//
//}
//+ (void)callLogin:(NSString*)strUser andPass:(NSString *)strPass OnCompletion:(FetchCompletionHandler)handler{
//    SSRestManager *restManager = [[SSRestManager alloc] init];
//    NSString *menuUrl;
//    menuUrl = [NSString stringWithFormat:@"%@%@",kBaseURL,@"webservice_appuserlogin"];
//    NSString *strQuery = [NSString stringWithFormat:@"username=%@&password=%@",strUser,strPass];
//    [restManager getJsonResponseFromBaseUrl:menuUrl query:strQuery onCompletion:^(NSDictionary *json) {
//        if (json) {
//            NSInteger status = [[json valueForKey:@"status"]integerValue];
//            NSString *strStatus = [NSString stringWithFormat:@"%ld",(long)status];
//            if([strStatus isEqualToString:@"1"]){
//            //store user value in ios
//           AppDelegate *appdelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
//            NSString *result = [json valueForKey:@"result"];
//            appdelegate.strUid = [result valueForKey:@"id"];
//            NSString *uname = [result valueForKey:@"user_login"];
//            [[NSUserDefaults standardUserDefaults] setObject:uname forKey:@"uname"];
//            [[NSUserDefaults standardUserDefaults] setObject:appdelegate.strUid forKey:@"uid"];
//            [[NSUserDefaults standardUserDefaults] synchronize];
//            }
//            handler(strStatus,nil);
//        }
//    } onError:^(NSError *error) {
//        if (error) {
//            handler(nil,error);
//        }
//    }];
//
//}
//+ (void)createQueue:(NSString*)strQueueName OnCompletion:(FetchCompletionHandler)handler{
//    SSRestManager *restManager = [[SSRestManager alloc] init];
//    NSString *menuUrl;
//    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
//    // getting an NSString
//    NSString *uid = [prefs stringForKey:@"uid"];
//
//    menuUrl = [NSString stringWithFormat:@"%@%@",kBaseURL,@"webservice_addqueue"];
//    NSString *strQuery = [NSString stringWithFormat:@"q_name=%@&id=%@",strQueueName,uid];
//    [restManager getJsonResponseFromBaseUrl:menuUrl query:strQuery onCompletion:^(NSDictionary *json) {
//        if (json) {
//            NSInteger status = [[json valueForKey:@"status"]integerValue];
//            NSString *strStatus = [NSString stringWithFormat:@"%ld",(long)status];
//            if([strStatus isEqualToString:@"1"]){
//                //store user value in ios
//                NSString *result = [json valueForKey:@"result"];
//                NSString *strQueue = [result valueForKey:@"queueid"];
//                handler(strQueue,nil);
//            }           
//        }
//    } onError:^(NSError *error) {
//        if (error) {
//            handler(nil,error);
//        }
//    }];
//
//}
//+ (void)addVideoToQueue:(NSString*)strVidName withQueue:(NSString*)strQueueId OnCompletion:(FetchCompletionHandler)handler{
//    SSRestManager *restManager = [[SSRestManager alloc] init];
//    NSString *menuUrl;
//    //strVidName=@"http://www.youtube.com/watch?v=6y71y2aTR5k";
//    
//    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
//    NSString *uid = [prefs stringForKey:@"uid"];
//    menuUrl = [NSString stringWithFormat:@"%@%@",kBaseURL,@"webservice_addvideo_queue"];
//    NSString *strQuery = [NSString stringWithFormat:@"queueid=%@&txtLink=%@&id=%@",strQueueId,strVidName,uid];
//    [restManager getJsonResponseFromBaseUrl:menuUrl query:strQuery onCompletion:^(NSDictionary *json) {
//        if (json) {
//            NSInteger status = [[json valueForKey:@"status"]integerValue];
//            NSString *strStatus = [NSString stringWithFormat:@"%ld",(long)status];
//            if([strStatus isEqualToString:@"1"]){
//                //store user value in ios
//                handler(strStatus,nil);
//            }else{
//                handler(strStatus,nil);
//            }
//            
//        }
//    } onError:^(NSError *error) {
//        if (error) {
//            handler(nil,error);
//        }
//    }];
//
//}
//+(void)fetchQueueListOnCompletion:(NSString*)strUserId  OnCompletion:(FetchCompletionHandler)handler{
//    SSRestManager *restManager = [[SSRestManager alloc] init];
//    NSString *menuUrl;
//    menuUrl = [NSString stringWithFormat:@"%@%@",kBaseURL,@"webservice_userqueuelist"];
//    NSString *strQuery = [NSString stringWithFormat:@"id=%@",strUserId];
//    [restManager getJsonResponseFromBaseUrl:menuUrl query:strQuery onCompletion:^(NSDictionary *json) {
//        if (json) {
//            NSInteger status = [[json valueForKey:@"status"]integerValue];
//            NSString *strStatus = [NSString stringWithFormat:@"%ld",(long)status];
//            NSMutableArray *arrQueueList = [[NSMutableArray alloc]init];
//            
//            if([strStatus isEqualToString:@"1"]){
//                NSArray *arrResult = [json valueForKey:@"result"];
//                [arrResult enumerateObjectsUsingBlock:^(NSDictionary *dict, NSUInteger idx, BOOL *stop) {
//                    Queue *queue = [[Queue alloc]init];
//                    queue.strQueueID = [dict valueForKey:@"queueid"];
//                    queue.strQueueCreatedBy = [dict valueForKey:@"authorname"];
//                    queue.strQueueCreatedDate = [dict valueForKey:@"createdate"];
//                    queue.strQueueScore = [dict valueForKey:@"score"];
//                    queue.strQueueThumbLink = [dict valueForKey:@"image"];
//                    queue.strQueueTitle = [dict valueForKey:@"title"];
//                    [arrQueueList addObject:queue];
//
//                }];
//                
//                //store user value in ios
//                handler(arrQueueList,nil);
//            }else{
//                handler(arrQueueList,nil);
//            }
//            
//        }
//    } onError:^(NSError *error) {
//        if (error) {
//            handler(nil,error);
//        }
//    }];
//
//}
//+ (void)fetchMyVideoList:(NSString*)strQueueId OnCompletion:(FetchCompletionHandler)handler{
//    SSRestManager *restManager = [[SSRestManager alloc] init];
//    NSString *menuUrl;
//     menuUrl = [NSString stringWithFormat:@"%@%@",kBaseURL,@"webservice_userqueuedetaillist"];
//    strQueueId = [NSString stringWithFormat:@"queueid=%@",strQueueId];
//    [restManager getJsonResponseFromBaseUrl:menuUrl query:strQueueId onCompletion:^(NSDictionary *json) {
//        if (json) {
//            NSMutableArray *vidFullList = [[NSMutableArray alloc] init];
//            NSInteger status = [[json valueForKey:@"status"]integerValue];
//            if(status==1){
//                NSArray *vidList = [json valueForKey:@"result"];
//                [vidList enumerateObjectsUsingBlock:^(NSDictionary *vidDict, NSUInteger idx, BOOL *stop) {
//                    VideoDetails *vid= [[VideoDetails alloc]initWithDict:vidDict];
//                    [vidFullList addObject:vid];
//                    
//                }];
//                
//                handler(vidFullList,nil);
//            }else{
//                handler(vidFullList,nil);
//            }
//        }
//    } onError:^(NSError *error) {
//        if (error) {
//            handler(nil,error);
//        }
//    }];
//
//}
@end
