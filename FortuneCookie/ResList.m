//
//  ResList.m
//  FortuneCookie
//
//  Created by Aditi Manna on 09/11/15.
//  Copyright © 2015 Limtex Infotech. All rights reserved.
//

#import "ResList.h"
#import "DinnerTime.h"
#import "LunchTime.h"
@implementation ResList
- (id)initWithDict:(NSDictionary *)dict withResList:(BOOL)yesRes{
    if (self = [super init]) {
        if(yesRes==YES){
        self.strResId = [dict objectForKey:@"id"];
        self.strResName = [dict objectForKey:@"name"];
        self.strAddr = [dict objectForKey:@"address_1"];
        if([self.strAddr isEqualToString:@""]){
             self.strAddr = @"123 North Street Adelaide 5000";
            
        }
        self.strCity = [dict objectForKey:@"city"];
        self.strLatt = [dict objectForKey:@"latitude"];
        self.strLongi = [dict objectForKey:@"longitude"];
        self.strCat = [dict objectForKey:@"type"];
        }else{
            self.strResId = [dict objectForKey:@"id"];
            self.strResName = [dict objectForKey:@"name"];
            self.strAddr = [dict objectForKey:@"address_1"];
            self.strAddr2 = [dict objectForKey:@"address_2"];
            if([self.strAddr isEqualToString:@""]){
                self.strAddr = @"123 North Street Adelaide 5000";
                
            }
            self.strphn =[dict objectForKey:@"ph_no"];
            self.strCity = [dict objectForKey:@"city"];
            self.strCountry = [dict objectForKey:@"country"];
            self.strdesc = [dict objectForKey:@"description"];
            self.strEmail = [dict objectForKey:@"email"];
            
            self.strFbUrl = [dict objectForKey:@"facebook_url"];
            self.strFeature = [dict objectForKey:@"features"];
            self.strFbID = [dict objectForKey:@"facebook_id"];
             self.strTwitter = [dict objectForKey:@"twitter_url"];
             self.strImg = [dict objectForKey:@"image"];
            self.arrDinner = [[NSMutableArray alloc]init];
            NSMutableArray *arrDinnerT = [dict objectForKey:@"dinner_time"];
            [arrDinnerT enumerateObjectsUsingBlock:^(NSDictionary *dict, NSUInteger idx, BOOL * _Nonnull stop) {
                DinnerTime *dt = [[DinnerTime alloc]init];
                dt.strDinnerDay = [dict valueForKey:@"day"];
                dt.strDinnerTime = [dict valueForKey:@"dinner_time"];
                [self.arrDinner addObject:dt];
            }];
            
            
            self.arrLunch = [[NSMutableArray alloc]init];
            NSMutableArray *arrLunchTime = [dict objectForKey:@"dinner_time"];
            [arrLunchTime enumerateObjectsUsingBlock:^(NSDictionary *dict, NSUInteger idx, BOOL * _Nonnull stop) {
                LunchTime *lt = [[LunchTime alloc]init];
                lt.strLunchDay = [dict valueForKey:@"day"];
                lt.strLunchTime = [dict valueForKey:@"lunch_time"];
                [self.arrLunch addObject:lt];
            }];

            self.strLatt = [dict objectForKey:@"latitude"];
            self.strLongi = [dict objectForKey:@"longitude"];
            self.strCat = [dict objectForKey:@"type"];
        }
       
    }
    return self;
}
@end
