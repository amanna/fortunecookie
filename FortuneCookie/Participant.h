//
//  Participant.h
//  FortuneCookie
//
//  Created by Aditi Manna on 09/12/15.
//  Copyright © 2015 Limtex Infotech. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Participant : NSObject
@property(nonatomic)NSString *strImage;
@property(nonatomic)bool yesIm;
@property(nonatomic)NSString *strUname;
@property(nonatomic)NSString *strBool;
- (id)initWithDict:(NSDictionary *)dict;
@end
