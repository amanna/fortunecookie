//
//  LunchTime.h
//  FortuneCookie
//
//  Created by Aditi Manna on 13/11/15.
//  Copyright © 2015 Limtex Infotech. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LunchTime : NSObject
@property(nonatomic)NSString *strLunchDay;
@property(nonatomic)NSString *strLunchTime;
- (id)initWithDict:(NSDictionary *)dict;
@end
