//
//  SelectState.m
//  FortuneCookie
//
//  Created by Aditi Manna on 09/10/15.
//  Copyright © 2015 Limtex Infotech. All rights reserved.
//

#import "SelectState.h"
#import "AppDelegate.h"
#import "WebserviceManager.h"
#import "StateList.h"
#import "LGViewHUD.h"
#import "UtilityManager.h"
@interface SelectState (){
    AppDelegate *appdel;
    
}
@property(nonatomic)NSMutableArray *arrState;
@property (weak, nonatomic) IBOutlet UITableView *tblView;

@end

@implementation SelectState

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tblView.hidden = YES;
    LGViewHUD* hud = [LGViewHUD defaultHUD];
    hud.activityIndicatorOn = YES;
    hud.topText = @"";
    hud.bottomText = @"";
    [hud showInView:self.view];
    [WebserviceManager callStateList:^(id object, NSError *error) {
        [[LGViewHUD defaultHUD] hideWithAnimation:HUDAnimationHideFadeOut];
        if(object){
            self.tblView.hidden = NO;
            
            self.arrState = object;
            [self.tblView reloadData];
//            self.tblView.userInteractionEnabled = YES;
        }else{
            [UtilityManager showAlertWithMessage:@"Some Problem Occurs!!Please try again later"];
        }
    }];
    
     
    
//    self.arrState = [[NSMutableArray alloc]initWithObjects:@"Australian Capital Territory",@"New South Wales", @"Northern Territory",@"Queensland",@"South Australia",@"Tasmania",@"Victoria",nil];
   
    // Do any additional setup after loading the view.
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.arrState.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
   cell.selectionStyle = UITableViewCellSelectionStyleNone;
    StateList *st = [self.arrState objectAtIndex:indexPath.row];
    cell.textLabel.text = st.strStateName;
    cell.textLabel.textColor = [UIColor whiteColor];
    return cell;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
   // [self performSegueWithIdentifier:@"segueMap" sender:self];
   
    StateList *st = [self.arrState objectAtIndex:indexPath.row];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];  //load NSUserDefaults
    NSString *stateId = st.strStateId;
    [prefs setObject:stateId forKey:@"state"];
    
    //set the prev Array for key value "favourites"
    if([self.strType isEqualToString:@"settings"]){
        [self dismissViewControllerAnimated:YES completion:nil];
    }else{
        [self performSegueWithIdentifier:@"segueLogin" sender:self];
    }
    

}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
