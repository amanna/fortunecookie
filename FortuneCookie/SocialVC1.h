//
//  SocialVC1.h
//  FortuneCookie
//
//  Created by Aditi Manna on 27/11/15.
//  Copyright © 2015 Limtex Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SocialVC1 : UIViewController
@property (weak, nonatomic) IBOutlet UIWebView *myweb;
- (IBAction)btnCloseAction:(UIButton *)sender;
@property(nonatomic)NSString *strUrl;
@end
