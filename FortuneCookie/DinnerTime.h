//
//  DinnerTime.h
//  FortuneCookie
//
//  Created by Aditi Manna on 13/11/15.
//  Copyright © 2015 Limtex Infotech. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DinnerTime : NSObject
@property(nonatomic)NSString *strDinnerDay;
@property(nonatomic)NSString *strDinnerTime;
- (id)initWithDict:(NSDictionary *)dict;
@end
