//
//  ViewController.m
//  CollisionEffect
//
//  Created by GauravKumar on 07/10/15.
//  Copyright © 2015 GauravKumar. All rights reserved.
//

#import "MyDrawLiveVC.h"
#import "WebserviceManager.h"
#import "Participant.h"
@interface MyDrawLiveVC ()

@property (nonatomic, strong) UIDynamicAnimator *animator;
@property (nonatomic, strong) UIDynamicItemBehavior *viewBehaviour;

@property (nonatomic, strong) UIView *orangeBall;
@property (nonatomic, strong) UIView *blueBall;
@property (nonatomic, strong) UIView *redBall;
@property (nonatomic, strong) UIView *greenBall;
@property (nonatomic, strong) UIView *grayBall;
@property (nonatomic, strong) UIView *yellowBall;
@property (nonatomic, strong) UIView *purpleBall;
@property (nonatomic, strong) UIView *blackBall;
@property (weak, nonatomic) IBOutlet UIView *ballCollectionView;

@property(nonatomic)NSMutableArray *arrBall;
@property (nonatomic, strong) UIGravityBehavior *gravityBehavior;
@property (weak, nonatomic) IBOutlet UILabel *lblResNmae;
@property (weak, nonatomic) IBOutlet UILabel *lblDraw;

@end

@implementation MyDrawLiveVC{
    
    NSTimer *myTimer;
}
- (void)viewDidAppear:(BOOL)animated{
    self.arrBall = [[NSMutableArray alloc]init];
    
    NSString *str = self.strDrawDt;
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    NSDate *date = [dateFormatter dateFromString: str];
    
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
    NSString *convertedString = [dateFormatter stringFromDate:date];
    
    self.lblDraw.text = convertedString;
    self.lblResNmae.text = self.strResName;
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    // getting an NSString
    NSString *strUser = [prefs stringForKey:@"uid"];

    [WebserviceManager callDrawDetails:self.strDrawDt andUserId:strUser andResId:self.strResId  OnCompletion:^(id object, NSError *error) {
        if(object){
            self.arrBall = object;
            
            
            for(int i=0; i<self.arrBall.count; i++){
                Participant *part = [self.arrBall objectAtIndex:i];
                dispatch_async(dispatch_get_global_queue(0,0), ^{
                    NSData * data = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString:part.strImage]];
                    if ( data != nil )
                        
                    dispatch_async(dispatch_get_main_queue(), ^{
                       
                        
                        UILabel *nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(15, 40, 45, 25)];
                        [nameLabel setFont:[UIFont fontWithName:@"Helvetica" size:14]];
                        nameLabel.text = part.strUname;
                        
                        
                        UIImage *participantImage = [UIImage imageNamed:@"user_02.png"];
                        UIImageView *participantImageView = [[UIImageView alloc] initWithImage:[UIImage imageWithData: data]];
                        participantImageView.frame = CGRectMake(15, 5, 40, 40);
                        
                        if(i==0){
                        
                        self.orangeBall = [[UIView alloc] initWithFrame:CGRectMake(100.0, 400.0, 70.0, 70.0)];
                        self.orangeBall.layer.cornerRadius = 35.0;
                        self.orangeBall.layer.borderColor = [UIColor blackColor].CGColor;
                        self.orangeBall.layer.borderWidth = 0.0;
                        [self.orangeBall setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bubble_nc"]]];
                        
                        // add the imageview to the superview
                        [self.orangeBall addSubview:participantImageView];
                        [self.orangeBall addSubview:nameLabel];
                        [self.view addSubview:self.orangeBall];
                        }else if (i==1){
                        
                        // Setup the ball view.
                        self.blueBall = [[UIView alloc] initWithFrame:CGRectMake(10.0, 400.0, 70.0, 70.0)];
                        self.blueBall.layer.cornerRadius = 35.0;
                        self.blueBall.layer.borderColor = [UIColor blackColor].CGColor;
                        self.blueBall.layer.borderWidth = 0.0;
                        [self.blueBall setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bubble_nc"]]];
                        
                        UIImageView *participantImageView2 = [[UIImageView alloc] initWithImage:participantImage];
                        participantImageView2.frame = CGRectMake(15, 5, 40, 40);
                        [self.blueBall addSubview:participantImageView2];
                        [self.view addSubview:self.blueBall];
                            
                        }else if (i==2){
                        
                        self.redBall = [[UIView alloc] initWithFrame:CGRectMake(20.0, 400.0, 70.0, 70.0)];
                        self.redBall.backgroundColor = [UIColor redColor];
                        self.redBall.layer.cornerRadius = 35.0;
                        self.redBall.layer.borderColor = [UIColor blackColor].CGColor;
                        self.redBall.layer.borderWidth = 0.0;
                        [self.redBall setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bubble_nc"]]];
                        
                        UIImageView *participantImageView3 = [[UIImageView alloc] initWithImage:participantImage];
                        participantImageView3.frame = CGRectMake(15, 5, 40, 40);
                        [self.redBall addSubview:participantImageView3];
                        [self.view addSubview:self.redBall];
                            
                        }else if (i==3){
                        
                        self.greenBall = [[UIView alloc] initWithFrame:CGRectMake(30.0, 400.0, 70.0, 70.0)];
                        self.greenBall.backgroundColor = [UIColor greenColor];
                        self.greenBall.layer.cornerRadius = 35.0;
                        self.greenBall.layer.borderColor = [UIColor blackColor].CGColor;
                        self.greenBall.layer.borderWidth = 0.0;
                        [self.greenBall setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bubble_sc"]]];
                        [self.greenBall addSubview:participantImageView];
                        [self.view addSubview:self.greenBall];
                         }else if (i==4){
                        self.purpleBall = [[UIView alloc] initWithFrame:CGRectMake(40.0, 400.0, 70.0, 70.0)];
                        self.purpleBall.backgroundColor = [UIColor purpleColor];
                        self.purpleBall.layer.cornerRadius = 35.0;
                        self.purpleBall.layer.borderColor = [UIColor blackColor].CGColor;
                        self.purpleBall.layer.borderWidth = 0.0;
                        [self.purpleBall setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bubble_nc"]]];
                        
                        UIImageView *participantImageView4 = [[UIImageView alloc] initWithImage:participantImage];
                        participantImageView4.frame = CGRectMake(15, 5, 40, 40);
                        
                        [self.purpleBall addSubview:participantImageView4];
                        [self.view addSubview:self.purpleBall];
                         }else if (i==5){
                        
                        self.grayBall = [[UIView alloc] initWithFrame:CGRectMake(50.0, 400.0, 70.0, 70.0)];
                        self.grayBall.backgroundColor = [UIColor grayColor];
                        self.grayBall.layer.cornerRadius = 35.0;
                        self.grayBall.layer.borderColor = [UIColor blackColor].CGColor;
                        self.grayBall.layer.borderWidth = 0.0;
                        [self.grayBall setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bubble_nc"]]];
                        
                        UIImageView *participantImageView5 = [[UIImageView alloc] initWithImage:participantImage];
                        participantImageView5.frame = CGRectMake(15, 5, 40, 40);
                        
                        [self.grayBall addSubview:participantImageView5];
                        [self.view addSubview:self.grayBall];
                        }else if (i==6){
                        self.yellowBall = [[UIView alloc] initWithFrame:CGRectMake(60.0, 400.0, 70.0, 70.0)];
                        self.yellowBall.backgroundColor = [UIColor yellowColor];
                        self.yellowBall.layer.cornerRadius = 35.0;
                        self.yellowBall.layer.borderColor = [UIColor blackColor].CGColor;
                        self.yellowBall.layer.borderWidth = 0.0;
                        [self.yellowBall setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bubble_nc"]]];
                        
                        UIImageView *participantImageView6 = [[UIImageView alloc] initWithImage:participantImage];
                        participantImageView6.frame = CGRectMake(15, 5, 40, 40);
                        
                        [self.yellowBall addSubview:participantImageView6];
                        [self.view addSubview:self.yellowBall];
                        }else{
                        self.blackBall = [[UIView alloc] initWithFrame:CGRectMake(70.0, 400.0, 70.0, 70.0)];
                        self.blackBall.backgroundColor = [UIColor blackColor];
                        self.blackBall.layer.cornerRadius = 35.0;
                        self.blackBall.layer.borderColor = [UIColor blackColor].CGColor;
                        self.blackBall.layer.borderWidth = 0.0;
                        [self.blackBall setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bubble_nc"]]];
                        
                        UIImageView *participantImageView7 = [[UIImageView alloc] initWithImage:participantImage];
                        participantImageView7.frame = CGRectMake(15, 5, 40, 40);
                        
                        [self.blackBall addSubview:participantImageView7];
                        [self.view addSubview:self.blackBall];
                        }
                        if(i==self.arrBall.count - 1){
                        // Initialize the animator.
                        self.animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
                        
                        self.viewBehaviour = [[UIDynamicItemBehavior alloc] initWithItems:@[self.ballCollectionView]];
                        self.viewBehaviour.allowsRotation = NO;
                        self.viewBehaviour.density = 1000.0f;
                        [self.animator addBehavior:self.viewBehaviour];
                        
                        [self demoGravity];
                        
                        myTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self
                                                                 selector:@selector(changeGravityDirection) userInfo:nil repeats:YES];
                        }
                    });
                });

            }
           

        }else{
            
        }
    }];
    
   

}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    // Setup the ball view.
    

    
    
}

-(void)demoGravity{
    UICollisionBehavior* collision;
    UIDynamicItemBehavior *ballBehavior;
    if(self.arrBall.count == 1){
        self.gravityBehavior = [[UIGravityBehavior alloc] initWithItems:@[self.orangeBall]];
      collision = [[UICollisionBehavior alloc]
                                                      initWithItems:@[self.orangeBall,self.ballCollectionView]];
        ballBehavior = [[UIDynamicItemBehavior alloc] initWithItems:@[self.orangeBall]];
    }else if (self.arrBall.count == 2){
        self.gravityBehavior = [[UIGravityBehavior alloc] initWithItems:@[self.orangeBall,self.blueBall]];
         collision = [[UICollisionBehavior alloc]
                                                      initWithItems:@[self.orangeBall,self.blueBall,self.ballCollectionView]];
        ballBehavior = [[UIDynamicItemBehavior alloc] initWithItems:@[self.orangeBall,self.blueBall]];

    }else if (self.arrBall.count == 3){
        self.gravityBehavior = [[UIGravityBehavior alloc] initWithItems:@[self.orangeBall,self.blueBall,self.redBall]];
       collision = [[UICollisionBehavior alloc]
                                                      initWithItems:@[self.orangeBall,self.blueBall,self.redBall,self.ballCollectionView]];
        ballBehavior = [[UIDynamicItemBehavior alloc] initWithItems:@[self.orangeBall,self.blueBall,self.redBall]];
    }else if (self.arrBall.count == 4){
        self.gravityBehavior = [[UIGravityBehavior alloc] initWithItems:@[self.orangeBall,self.blueBall,self.redBall,self.greenBall]];
        collision = [[UICollisionBehavior alloc]
                                                      initWithItems:@[self.orangeBall,self.blueBall,self.redBall,self.greenBall,self.ballCollectionView]];
        ballBehavior = [[UIDynamicItemBehavior alloc] initWithItems:@[self.orangeBall,self.blueBall,self.redBall,self.greenBall]];

    }else if (self.arrBall.count == 5){
        self.gravityBehavior = [[UIGravityBehavior alloc] initWithItems:@[self.orangeBall,self.blueBall,self.redBall,self.purpleBall,self.greenBall]];
        collision = [[UICollisionBehavior alloc]
                                                      initWithItems:@[self.orangeBall,self.blueBall,self.redBall,self.purpleBall,self.greenBall,self.ballCollectionView]];
        ballBehavior = [[UIDynamicItemBehavior alloc] initWithItems:@[self.orangeBall,self.blueBall,self.redBall,self.purpleBall,self.greenBall]];

    }else if (self.arrBall.count == 6){
        self.gravityBehavior = [[UIGravityBehavior alloc] initWithItems:@[self.orangeBall,self.blueBall,self.redBall,self.purpleBall,self.grayBall,self.greenBall]];
        collision = [[UICollisionBehavior alloc]
                                                      initWithItems:@[self.orangeBall,self.blueBall,self.redBall,self.purpleBall,self.grayBall,self.greenBall,self.ballCollectionView]];
       ballBehavior = [[UIDynamicItemBehavior alloc] initWithItems:@[self.orangeBall,self.blueBall,self.redBall,self.purpleBall,self.grayBall,self.greenBall]];

    }else if (self.arrBall.count == 7){
        self.gravityBehavior = [[UIGravityBehavior alloc] initWithItems:@[self.orangeBall,self.blueBall,self.redBall,self.purpleBall,self.yellowBall,self.grayBall,self.greenBall]];
        collision = [[UICollisionBehavior alloc]
                                                      initWithItems:@[self.orangeBall,self.blueBall,self.redBall,self.purpleBall,self.yellowBall,self.grayBall,self.greenBall,self.ballCollectionView]];
        ballBehavior = [[UIDynamicItemBehavior alloc] initWithItems:@[self.orangeBall,self.blueBall,self.redBall,self.purpleBall,self.yellowBall,self.grayBall,self.greenBall]];

    }else if (self.arrBall.count == 8){
        self.gravityBehavior = [[UIGravityBehavior alloc] initWithItems:@[self.orangeBall,self.blueBall,self.redBall,self.purpleBall,self.yellowBall,self.grayBall,self.blackBall,self.greenBall]];
       collision = [[UICollisionBehavior alloc]
                                                      initWithItems:@[self.orangeBall,self.blueBall,self.redBall,self.purpleBall,self.yellowBall,self.grayBall,self.blackBall,self.greenBall,self.ballCollectionView]];
        ballBehavior = [[UIDynamicItemBehavior alloc] initWithItems:@[self.orangeBall,self.blueBall,self.redBall,self.purpleBall,self.yellowBall,self.grayBall,self.blackBall,self.greenBall,]];

    }else{
        self.gravityBehavior = [[UIGravityBehavior alloc] initWithItems:@[self.orangeBall,self.blueBall,self.redBall,self.purpleBall,self.yellowBall,self.grayBall,self.blackBall,self.greenBall]];
        collision = [[UICollisionBehavior alloc]
                                                      initWithItems:@[self.orangeBall,self.blueBall,self.redBall,self.purpleBall,self.yellowBall,self.grayBall,self.blackBall,self.greenBall,self.ballCollectionView]];
        ballBehavior = [[UIDynamicItemBehavior alloc] initWithItems:@[self.orangeBall,self.blueBall,self.redBall,self.purpleBall,self.yellowBall,self.grayBall,self.blackBall,self.greenBall,]];

    }
    
   
    [self.animator addBehavior:self.gravityBehavior];
    
   
    collision.translatesReferenceBoundsIntoBoundary = YES;
    [self.animator addBehavior:collision];
    collision.collisionMode = UICollisionBehaviorModeEverything;
    collision.collisionDelegate = (id)self;
    ballBehavior.elasticity = 1.0;
    [self.animator addBehavior:ballBehavior];
}

-(void)changeGravityDirection{
    
    NSArray *gravity = @[@0.1,@-0.1];
    
    NSUInteger randomIndex = arc4random() % [gravity count];
    [self.gravityBehavior setGravityDirection:CGVectorMake([[gravity objectAtIndex:randomIndex] doubleValue], [[gravity objectAtIndex:randomIndex] doubleValue])];
    
    [self.animator addBehavior:self.gravityBehavior];
}

-(void)collisionBehavior:(UICollisionBehavior *)behavior beganContactForItem:(id)item1 withItem:(id)item2 atPoint:(CGPoint)p{
    UIPushBehavior *pushBehavior;
    if(self.arrBall.count == 1){
    pushBehavior = [[UIPushBehavior alloc] initWithItems:@[self.orangeBall] mode:UIPushBehaviorModeInstantaneous];
    }else if (self.arrBall.count==2){
        
        pushBehavior = [[UIPushBehavior alloc] initWithItems:@[self.orangeBall,self.blueBall] mode:UIPushBehaviorModeInstantaneous];
    }else if (self.arrBall.count==3){
        pushBehavior = [[UIPushBehavior alloc] initWithItems:@[self.orangeBall,self.blueBall,self.redBall,self.greenBall] mode:UIPushBehaviorModeInstantaneous];
        
    }else if (self.arrBall.count==4){
        pushBehavior = [[UIPushBehavior alloc] initWithItems:@[self.orangeBall,self.blueBall,self.redBall,self.purpleBall,self.yellowBall,self.grayBall,self.blackBall,self.greenBall] mode:UIPushBehaviorModeInstantaneous];
        
    }else if (self.arrBall.count==5){
        pushBehavior = [[UIPushBehavior alloc] initWithItems:@[self.orangeBall,self.blueBall,self.redBall,self.purpleBall,self.yellowBall,self.grayBall,self.blackBall,self.greenBall] mode:UIPushBehaviorModeInstantaneous];
        
    }else if (self.arrBall.count==6){
        pushBehavior = [[UIPushBehavior alloc] initWithItems:@[self.orangeBall,self.blueBall,self.redBall,self.purpleBall,self.yellowBall,self.grayBall,self.blackBall,self.greenBall] mode:UIPushBehaviorModeInstantaneous];
        
    }else if (self.arrBall.count==7){
        pushBehavior = [[UIPushBehavior alloc] initWithItems:@[self.orangeBall,self.blueBall,self.redBall,self.purpleBall,self.yellowBall,self.grayBall,self.blackBall,self.greenBall] mode:UIPushBehaviorModeInstantaneous];
        
     }else if (self.arrBall.count==8){
         pushBehavior = [[UIPushBehavior alloc] initWithItems:@[self.orangeBall,self.blueBall,self.redBall,self.purpleBall,self.yellowBall,self.grayBall,self.blackBall,self.greenBall] mode:UIPushBehaviorModeInstantaneous];
     }else{
         pushBehavior = [[UIPushBehavior alloc] initWithItems:@[self.orangeBall,self.blueBall,self.redBall,self.purpleBall,self.yellowBall,self.grayBall,self.blackBall,self.greenBall] mode:UIPushBehaviorModeInstantaneous];
     }
        
    pushBehavior.angle = 0.0;
    
    NSArray *magnitudeValue = @[@0.1, @0.2, @-0.1, @-0.2];
    
    NSUInteger randomIndex = arc4random() % [magnitudeValue count];
    
    pushBehavior.magnitude =  [[magnitudeValue objectAtIndex:randomIndex] doubleValue];
    
    //[self.animator addBehavior:pushBehavior];
}

-(void)viewWillDisappear:(BOOL)animated{
    [myTimer invalidate];
     myTimer = nil;
     [super viewWillDisappear:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnBackAction:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
