//
//  ResList.h
//  FortuneCookie
//
//  Created by Aditi Manna on 09/11/15.
//  Copyright © 2015 Limtex Infotech. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ResList : NSObject
@property(nonatomic)NSString *strResId;
@property(nonatomic)NSString *strResName;
@property(nonatomic)NSString *strAddr;
@property(nonatomic)NSString *strAddr2;
@property(nonatomic)NSString *strCity;
@property(nonatomic)NSString *strCountry;
@property(nonatomic)NSString *strdesc;
@property(nonatomic)NSString *strEmail;
@property(nonatomic)NSString *strFbUrl;
@property(nonatomic)NSString *strFbID;
@property(nonatomic)NSString *strFeature;
@property(nonatomic)NSString *strTwitter;
@property(nonatomic)NSString *strImg;
@property(nonatomic)NSString *strZip;
@property(nonatomic)NSString *strType;

@property(nonatomic)NSMutableArray *arrLunch;
@property(nonatomic)NSMutableArray *arrDinner;

@property(nonatomic)NSString *strLatt;
@property(nonatomic)NSString *strLongi;
@property(nonatomic)NSString *strCat;
@property(nonatomic)NSString *strphn;
- (id)initWithDict:(NSDictionary *)dict withResList:(BOOL)yesRes;
@end
