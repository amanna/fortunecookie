//
//  CTextField.m
//  FortuneCookie
//
//  Created by Aditi Manna on 14/10/15.
//  Copyright © 2015 Limtex Infotech. All rights reserved.
//

#import "CTextField.h"

@implementation CTextField

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (CGRect)textRectForBounds:(CGRect)bounds{
    return CGRectInset(bounds , 10, 10);
}
- (CGRect)editingRectForBounds:(CGRect)bounds{
     return CGRectInset(bounds , 10, 10);
}
@end
