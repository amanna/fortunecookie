//
//  DrawConfirmVC.h
//  FortuneCookie
//
//  Created by Aditi Manna on 07/10/15.
//  Copyright © 2015 Limtex Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ResList.h"
@protocol drawdelegate <NSObject>
@required
- (void) drawCompleted:(NSInteger)btnTag;
@end
@interface DrawConfirmVC : UIViewController{
     id <drawdelegate> _delegate;
}
@property (nonatomic,retain) ResList *rest1;
@property (weak, nonatomic) IBOutlet UILabel *lblResName;
@property (nonatomic,strong) id delegate;
@property (weak, nonatomic) IBOutlet UILabel *lblResAddr;
@property (weak, nonatomic) IBOutlet UILabel *lblPhn;
@property(nonatomic) NSString *strDrawDt;
@property (weak, nonatomic) IBOutlet UILabel *lblDrawDt;

@end
