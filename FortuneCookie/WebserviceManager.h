//
//  WebserviceManager.h
//  VideoShare
//
//  Created by Aditi Manna on 11/06/15.
//  Copyright (c) 2015 Limtex Infotech. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef void (^FetchCompletionHandler)(id object, NSError *error);
typedef void (^FetchCompletionHandler1)(id object,id object1, NSError *error);
@interface WebserviceManager : NSObject

+ (void)callFBLogin:(NSDictionary*)dictFb OnCompletion:(FetchCompletionHandler)handler;
+ (void)callStateList:(FetchCompletionHandler)handler;
+ (void)callResList:(NSString*)strSearch OnCompletion:(FetchCompletionHandler)handler;
+ (void)callResDetails:(NSString*)strRes OnCompletion:(FetchCompletionHandler)handler;
+ (void)callSubmitCode:(NSString*)strRes withUser:(NSString*)strUser withUniqueCode:(NSString*)strUnique withOTP:(NSString*)strOtp OnCompletion:(FetchCompletionHandler1)handler;
+ (void)callMyDraw:(NSString*)strUser OnCompletion:(FetchCompletionHandler)handler;
+ (void)callDrawDetails:(NSString*)strDrawId andUserId:(NSString*)strUser andResId:(NSString*)strRes  OnCompletion:(FetchCompletionHandler)handler;
//+(void)fetchRecentVideoOnCompletion:(NSString*)strVideoType withPage:(NSInteger)pageNo OnCompletion:(FetchCompletionHandler)handler;
//+ (void)fetchVideoDataWithLink:(NSString*)strVideoType andQueueId:(NSString *)strQueueId OnCompletion:(FetchCompletionHandler)handler;
//+ (void)callLogin:(NSString*)strUser andPass:(NSString *)strPass OnCompletion:(FetchCompletionHandler)handler;
//
//+ (void)createQueue:(NSString*)strQueueName OnCompletion:(FetchCompletionHandler)handler;
//+ (void)addVideoToQueue:(NSString*)strVidName withQueue:(NSString*)strQueueId OnCompletion:(FetchCompletionHandler)handler;
//+(void)fetchQueueListOnCompletion:(NSString*)strUserId  OnCompletion:(FetchCompletionHandler)handler;
//
//+ (void)fetchMyVideoList:(NSString*)strQueueId OnCompletion:(FetchCompletionHandler)handler;

@end
