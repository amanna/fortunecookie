//
//  MyDrawCell.h
//  FortuneCookie
//
//  Created by Aditi Manna on 06/10/15.
//  Copyright © 2015 Limtex Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyDrawCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblStatus;
@property (weak, nonatomic) IBOutlet UILabel *lblResName;
@property (weak, nonatomic) IBOutlet UILabel *lblDrawDt;
@property (weak, nonatomic) IBOutlet UILabel *lblChance;

@end
