//
//  Participant.m
//  FortuneCookie
//
//  Created by Aditi Manna on 09/12/15.
//  Copyright © 2015 Limtex Infotech. All rights reserved.
//

#import "Participant.h"

@implementation Participant
- (id)initWithDict:(NSDictionary *)dict {
    if (self = [super init]) {
        self.strImage = [dict objectForKey:@"image"];
        self.strUname = [dict objectForKey:@"username"];
        self.strBool = [dict objectForKey:@"is_you"];
    }
    return self;
}
@end
