//
//  AppDelegate.m
//  FortuneCookie
//
//  Created by Aditi Manna on 02/10/15.
//  Copyright © 2015 Limtex Infotech. All rights reserved.
//

#import "AppDelegate.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "LoginVC.h"

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    // Parse initialization
           //[self createTab];
      // [PFFacebookUtils initializeFacebook];
    

    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    NSString* strUID =    [defaults objectForKey:@"uid"];
    NSString* strState =    [defaults objectForKey:@"state"];
    if(strUID!=nil && strState!=nil){
        [self createTab];
    }
    return YES;
}
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                         openURL:url
                                               sourceApplication:sourceApplication
                                                     annotation:annotation];
}
- (void)createTab{
    _customTabBarVC = [[SSTabBarController alloc] initWithNibName:@"SSTabBarController" bundle:nil];
    UIStoryboard *storyboard =
    [UIStoryboard storyboardWithName:@"Main"
                              bundle:nil];
    MapVC *mapvc =
    [storyboard instantiateViewControllerWithIdentifier:@"MapVC"];
    UINavigationController *mapvcnav = [[UINavigationController alloc]initWithRootViewController:mapvc];
    mapvcnav.navigationBarHidden = YES;
    
    SearchVC *searchvc =
    [storyboard instantiateViewControllerWithIdentifier:@"SearchVC"];
    UINavigationController *navsearch = [[UINavigationController alloc]initWithRootViewController:searchvc];
   // navsearch.navigationBar.translucent = YES;
    navsearch.navigationBarHidden = YES;
    MyDraws *mydraw =
    [storyboard instantiateViewControllerWithIdentifier:@"MyDraws"];
    UINavigationController *navDraw = [[UINavigationController alloc]initWithRootViewController:mydraw];
    navDraw.navigationBarHidden = YES;
    
    _customTabBarVC.viewControllers = [NSArray arrayWithObjects:navsearch,mapvcnav,navDraw, nil];
    self.window.rootViewController = _customTabBarVC;
    _customTabBarVC.selectedViewController = mapvcnav;
    [self.window makeKeyAndVisible];
    
}

-(void)removeTab
{
    UIStoryboard *storyboard =
    [UIStoryboard storyboardWithName:@"Main"
                              bundle:nil];
    LoginVC *mydraw =
    [storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
     self.window.rootViewController = mydraw;

//   for(UITabBar *vc in [self.window.rootViewController.views])
//   {
//       if([vc isKindOfClass:UITabBar.class])
//       {
//           [vc removeFromSuperview];
//       }
//       
//       
//   }
    
    
    
//    for(UIView *view in self.window.subviews){
//        if([view isKindOfClass:[UITabBar class]]){
//            view.hidden = YES;
//            break;
//        }
//    }
    
    
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
}
- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
