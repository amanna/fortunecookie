//
//  ResdetailsVC.m
//  FortuneCookie
//
//  Created by Aditi Manna on 05/10/15.
//  Copyright © 2015 Limtex Infotech. All rights reserved.
//

#import "ResdetailsVC.h"
#import "DrawConfirmVC.h"
#import "CTextField.h"
#import "WebserviceManager.h"
#import "LGViewHUD.h"
#import "UtilityManager.h"
#import "ResList.h"
#import "DinnerTime.h"
#import "LunchTime.h"
#import "SocialVC1.h"
@interface ResdetailsVC ()<UITextFieldDelegate>{
    LGViewHUD* hud;
}
- (IBAction)btnBackAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIScrollView *myscrollview;
@property (weak, nonatomic) IBOutlet CTextField *txtCode;

@property (weak, nonatomic) IBOutlet UILabel *lblMonday;
@property (weak, nonatomic) IBOutlet UILabel *lblTuesDay;
@property (weak, nonatomic) IBOutlet UILabel *lblWed;
@property (weak, nonatomic) IBOutlet UILabel *lblThurs;
@property (weak, nonatomic) IBOutlet UILabel *lblFri;
@property (weak, nonatomic) IBOutlet UILabel *lblSat;
@property (weak, nonatomic) IBOutlet UILabel *lblSun;

@property (weak, nonatomic) IBOutlet UILabel *timemon;
@property (weak, nonatomic) IBOutlet UILabel *timeTues;

@property (weak, nonatomic) IBOutlet UILabel *timewed;
@property (weak, nonatomic) IBOutlet UILabel *timeThurs;
@property (weak, nonatomic) IBOutlet UILabel *timeFri;

@property (weak, nonatomic) IBOutlet UILabel *timeSat;
@property (weak, nonatomic) IBOutlet UILabel *timeSun;

@property (weak, nonatomic) IBOutlet UILabel *lblfeature;
@property (weak, nonatomic) IBOutlet UILabel *lbldesc;
- (IBAction)btnFBAction:(UIButton *)sender;
- (IBAction)btnTwitterAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIImageView *imgResBack;

@end

@implementation ResdetailsVC
@synthesize resId,rest;
- (void)viewDidLoad {
    [super viewDidLoad];
    self.txtCode.autocorrectionType = UITextAutocorrectionTypeNo;
    hud = [LGViewHUD defaultHUD];
    hud.activityIndicatorOn = YES;
    hud.topText = @"";
    hud.bottomText = @"";
    [hud showInView:self.view];
    
    [WebserviceManager callResDetails:self.resId OnCompletion:^(id object, NSError *error) {
        
        if(object){
            [self.txtCode setValue:[UIColor darkGrayColor]
                        forKeyPath:@"_placeholderLabel.textColor"];
            
            rest = object;
            NSString *imageUrl = rest.strImg;
            [NSURLConnection sendAsynchronousRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:imageUrl]] queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                [hud hideWithAnimation:HUDAnimationNone];
                self.imgResBack.image = [UIImage imageWithData:data];
                if(self.imgResBack.image == nil){
                     self.imgResBack.image = [UIImage imageNamed:@"noimg.png"];
                }
                _nameLabel.text=rest.strResName;
                _addressLabel.text=rest.strAddr;
                
                if(![rest.strResId isEqualToString:@"10"]){
                    _phoneLabel.text=@"9999999999";
                    _descLabel.text=@"Good food";
                }else{
                    
                    
                    _phoneLabel.text=rest.strphn;
                    _descLabel.text= rest.strdesc;
                    
                    self.lblfeature.text = rest.strFeature;
                    self.lbldesc.text = rest.strdesc;
                    
                    
                    
                    //populate dinnertime
                    
                    for(int i =0; i< rest.arrDinner.count; i++){
                        DinnerTime *dt = [rest.arrDinner objectAtIndex:i];
                        switch (i) {
                            case 0:{
                                self.lblMonday.text = dt.strDinnerDay;
                                self.timemon.text = dt.strDinnerTime;
                            }
                                break;
                            case 1:{
                                self.lblTuesDay.text = dt.strDinnerDay;
                                self.timeTues.text = dt.strDinnerTime;
                            }
                                break;
                            case 2:{
                                self.lblWed.text = dt.strDinnerDay;
                                self.timewed.text = dt.strDinnerTime;
                            }
                                break;
                            case 3:{
                                self.lblThurs.text = dt.strDinnerDay;
                                self.timeThurs.text = dt.strDinnerTime;
                            }
                                break;
                            case 4:{
                                self.lblFri.text = dt.strDinnerDay;
                                self.timeFri.text = dt.strDinnerTime;
                            }
                                break;
                            default:{
                                self.lblSat.text = dt.strDinnerDay;
                                self.timeSat.text = dt.strDinnerTime;
                            }
                                break;
                        }
                    }
                    
                }
            }];
            
                           
            
            
        }else{
            [UtilityManager showAlertWithMessage:@"Some Problem Occurs!!Please try again later"];
        }
    }];
    
    
   
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if(textField == self.txtCode){
        [self.txtCode resignFirstResponder];
    }
    return YES;
}
//-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
//    NSLog(@"%@", [alertView textFieldAtIndex:0].text);
//    
//    NSString *strOtp =[alertView textFieldAtIndex:0].text;
//    if(![strOtp isEqualToString:@""]){
//        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
//        // getting an NSString
//        NSString *strUser = [prefs objectForKey:@"uid"];
//        [WebserviceManager callSubmitCode:self.resId withUser:strUser withUniqueCode:self.txtCode.text withOTP:strOtp OnCompletion:^(id object,id object1, NSError *error) {
//            if(object){
//                NSString *status = object;
//                if([status isEqualToString:@"1"]){
//                    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main"
//                                                                         bundle:nil];
//                    DrawConfirmVC *add =
//                    [storyboard instantiateViewControllerWithIdentifier:@"DrawConfirmVC"];
//                    add.rest1 = self.rest;
//                    add.strDrawDt = object1;
//                    [self presentViewController:add
//                                       animated:YES
//                                     completion:nil];
//                }else{
//                    NSString *str = object1;
//                    [UtilityManager showAlertWithMessage:str];
//                }
//                
//                
//            }else{
//                [UtilityManager showAlertWithMessage:@"Some Problem Occurs!!Please try again later"];
//            }
//            
//        }];
//    }else{
//        [UtilityManager showAlertWithMessage:@"OTP can't be blank"];
//    }
//}

- (IBAction)btnSubmitAction:(id)sender {
     [self.txtCode resignFirstResponder];
    if(![self.txtCode.text isEqualToString:@""]){
        int textLength = [self.txtCode.text length];
        if(!textLength==10){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Code" message:@"Code must be 10 character"
                                                           delegate:self
                                                  cancelButtonTitle:@"Verify"
                                                  otherButtonTitles:@"Cancel",nil];
            
            [alert show];
        }else{
            NSString *ucode=[self.txtCode.text substringToIndex:5];
            NSString *otp = [self.txtCode.text substringFromIndex: [self.txtCode.text length] - 5];
            NSCharacterSet* notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
            if ([otp rangeOfCharacterFromSet:notDigits].location == NSNotFound)
            {
                NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                // getting an NSString
                NSString *strUser = [prefs objectForKey:@"uid"];
                [WebserviceManager callSubmitCode:self.resId withUser:strUser withUniqueCode:self.txtCode.text withOTP:@"" OnCompletion:^(id object,id object1, NSError *error) {
                    if(object){
                        NSString *status = object;
                        if([status isEqualToString:@"1"]){
                            UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main"
                                                                                 bundle:nil];
                            DrawConfirmVC *add =
                            [storyboard instantiateViewControllerWithIdentifier:@"DrawConfirmVC"];
                            add.rest1 = self.rest;
                            add.strDrawDt = object1;
                            [self presentViewController:add
                                               animated:YES
                                             completion:nil];
                        }else{
                            NSString *str = object1;
                            [UtilityManager showAlertWithMessage:str];
                        }
                        
                        
                    }else{
                        [UtilityManager showAlertWithMessage:@"Some Problem Occurs!!Please try again later"];
                    }
                    
                }];
                
            }else{
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Code" message:@"OTP must be numeric"
                                                               delegate:self
                                                      cancelButtonTitle:@"Verify"
                                                      otherButtonTitles:@"Cancel",nil];
                
                [alert show];

            }

        }
        
      
      
         }else{
       [UtilityManager showAlertWithMessage:@"Code can't be blank"];
   }
 
    
        // [self performSegueWithIdentifier:@"segueDrawConfirm" sender:self];
    //segueDrawConfirm
}
- (IBAction)btnBackAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
//    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)btnFBAction:(UIButton *)sender {
    if([self.rest.strFbUrl isEqualToString:@""]){
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"NO Facebook Link in this Restaurant" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alertView show];
    }else{
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle:nil];

        SocialVC1 *add =
        [storyboard instantiateViewControllerWithIdentifier:@"SocialVC1"];
        add.strUrl = self.rest.strFbUrl;
        
        [self presentViewController:add
                           animated:YES
                         completion:nil];
    }
}

- (IBAction)btnTwitterAction:(UIButton *)sender {
    if([self.rest.strFbUrl isEqualToString:@""]){
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"NO Twitter Link in this Restaurant" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alertView show];
    }else{
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle:nil];
        
        SocialVC1 *add =
        [storyboard instantiateViewControllerWithIdentifier:@"SocialVC1"];
        add.strUrl = self.rest.strTwitter;
        
        [self presentViewController:add
                           animated:YES
                         completion:nil];
    }
}
@end
