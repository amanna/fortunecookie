//
//  SearchVC.h
//  FortuneCookie
//
//  Created by Aditi Manna on 06/10/15.
//  Copyright © 2015 Limtex Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchVC : UIViewController
- (IBAction)btnBackAction:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *tblView;

@end
