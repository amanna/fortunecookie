//
//  SearchVCCell.h
//  FortuneCookie
//
//  Created by Aditi Manna on 06/10/15.
//  Copyright © 2015 Limtex Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchVCCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblResNmae;
@property (weak, nonatomic) IBOutlet UILabel *lblResCat;
@property (weak, nonatomic) IBOutlet UILabel *lblResAddr;
@property (weak, nonatomic) IBOutlet UILabel *lblChance;

@end
