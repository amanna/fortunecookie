//
//  SearchVC.m
//  FortuneCookie
//
//  Created by Aditi Manna on 06/10/15.
//  Copyright © 2015 Limtex Infotech. All rights reserved.
//

#import "SearchVC.h"
#import "SearchVCCell.h"
#import "WebserviceManager.h"
#import "ResList.h"
#import "UtilityManager.h"
#import "LGViewHUD.h"
#import "LoginVC.h"
#import "AppDelegate.h"
#import "ResdetailsVC.h"
@interface SearchVC ()<UITextFieldDelegate>
{
    AppDelegate *appdel;
    LGViewHUD* hud;
}

@property (weak, nonatomic) IBOutlet UITextField *txtSearch;
@property(nonatomic)NSMutableArray *arrRes;
@end

@implementation SearchVC
-(void)viewDidAppear:(BOOL)animated{
    [WebserviceManager callResList:@"" OnCompletion:^(id object, NSError *error) {
        [hud hideWithAnimation:HUDAnimationNone];
        if(object){
            NSLog(@"Object: %@",object);
            
            if([object count]==0){
                [self.tblView reloadData];
                [UtilityManager showAlertWithMessage:@"No restaurant found!!"];
            }
            else{
                self.tblView.hidden = NO;
                self.arrRes = object;
                [self.tblView reloadData];
            }
            
        }else{
            [UtilityManager showAlertWithMessage:@"Some Problem Occurs!!Please try again later"];
        }
    }];

}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.txtSearch.autocorrectionType = UITextAutocorrectionTypeNo;
    appdel = (AppDelegate *)[UIApplication sharedApplication].delegate;
    self.tblView.hidden = YES;
    hud = [LGViewHUD defaultHUD];
    hud.activityIndicatorOn = YES;
    hud.topText = @"";
    hud.bottomText = @"";
    [hud showInView:self.view];
    self.arrRes = [[NSMutableArray alloc]init];
    [self.txtSearch setValue:[UIColor darkGrayColor]
                    forKeyPath:@"_placeholderLabel.textColor"];
    
    
       

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.arrRes.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"Cell";
    
    SearchVCCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    ResList *res = [self.arrRes objectAtIndex:indexPath.row];
    cell.lblResNmae.text = res.strResName;
    cell.lblResCat.text = res.strCat;
    cell.lblResAddr.text = res.strAddr;
    //cell.lblResNmae.text = res.strResName;
    
    
//    if (cell == nil) {
//        cell = [[SearchVCCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
//    }
//    
//    cell.textLabel.text = @"Australian Capital";
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ResList *res = [self.arrRes objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:@"segueresdetails" sender:res];
}



- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    
    if([segue.identifier isEqualToString:@"segueresdetails"]) {
        ResList *res = sender;
        NSLog(@"ID %@",res.strResId);
        ResdetailsVC *controller = (ResdetailsVC *)segue.destinationViewController;
        controller.resId=res.strResId;
        //controller.rest=res;
        
    }
    
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if(textField==self.txtSearch){
        [self.txtSearch resignFirstResponder];
        [hud showInView:self.view];
        [WebserviceManager callResList:self.txtSearch.text OnCompletion:^(id object, NSError *error) {
            [hud hideWithAnimation:HUDAnimationNone];
            if(object){
                NSLog(@"Object: %@",object);
                
                if([object count]==0){
                    [UtilityManager showAlertWithMessage:@"No restaurant found!!"];
                }
                else{
                   
                    self.arrRes = object;
                    [self.tblView reloadData];
                }
                
            }else{
                [UtilityManager showAlertWithMessage:@"Some Problem Occurs!!Please try again later"];
            }
        }];

        
    }
    return YES;
}

- (IBAction)btnBackAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    
//    [appdel removeTab];
//    
//    LoginVC *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
//    [self.navigationController pushViewController:newView animated:YES];
}
@end
