//
//  MapVC.m
//  FortuneCookie
//
//  Created by Aditi Manna on 05/10/15.
//  Copyright © 2015 Limtex Infotech. All rights reserved.
//

#import "MapVC.h"
#import "SSTabBarController.h"
#import "SearchVC.h"
#import "MyDraws.h"
#import "AppDelegate.h"
#import "WebserviceManager.h"
#import "ResList.h"
#import "UtilityManager.h"
#import "LGViewHUD.h"
#import "MyAnnotation.h"
#import "ResdetailsVC.h"
@interface MapVC ()<tabbardelegate>{
    AppDelegate *appdel;
    NSString *resId;
}
@property(nonatomic)NSMutableArray *arrRes;

@end

@implementation MapVC

- (void)viewDidAppear:(BOOL)animated{
    resId=@"";
    
    appdel = (AppDelegate *)[UIApplication sharedApplication].delegate;
    LGViewHUD* hud = [LGViewHUD defaultHUD];
    hud.activityIndicatorOn = YES;
    hud.topText = @"";
    hud.bottomText = @"";
    [hud showInView:self.mapView];
    self.arrRes = [[NSMutableArray alloc]init];

    [WebserviceManager callResList:@"" OnCompletion:^(id object, NSError *error) {
        [hud hideWithAnimation:HUDAnimationNone];
        if(object){
            for(int i=0;i<[object count];i++)
            {
                ResList *res = [object objectAtIndex:i];
                NSLog(@"Object: %@ %@",res.strLatt,res.strLongi);
            }
            
            
            
            if([object count]==0){
                 [self.mapView removeAnnotations:self.mapView.annotations];
                [UtilityManager showAlertWithMessage:@"No restaurant found!!"];
            }
            else{
                self.arrRes = object;
                
                
               [self.mapView removeAnnotations:self.mapView.annotations];
                [self mapAnnotation];
            }
            
        }else{
            [UtilityManager showAlertWithMessage:@"Some Problem Occurs!!Please try again later"];
        }
    }];

}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    
    
    
    // Do any additional setup after loading the view.
}

-(void)mapAnnotation
{
    for ( int i=0; i<[self.arrRes count]; i++)
    {
        
        
        ResList *res = [self.arrRes objectAtIndex:i];
        ResList *res1 = [self.arrRes objectAtIndex:0];
        
        
        CLLocationCoordinate2D coord;
        
        coord.latitude=[[NSString stringWithFormat:@"%@",res.strLatt] floatValue];
        coord.longitude=[[NSString stringWithFormat:@"%@",
                          res.strLongi] floatValue];
        
        
        CLLocationCoordinate2D coord1;
        
        coord1.latitude=[[NSString stringWithFormat:@"%@",res1.strLatt] floatValue];
        coord1.longitude=[[NSString stringWithFormat:@"%@",
                          res1.strLongi] floatValue];
        
        
        MKCoordinateRegion region1;
        region1.center=coord1;
        region1.span.longitudeDelta=20 ;
        region1.span.latitudeDelta=20;
        [self.mapView setRegion:region1 animated:YES];
        
        NSString *titleStr =res.strResName ;
        // NSLog(@"title is:%@",titleStr);
        
//        MyAnnotation*  annotObj =[[MyAnnotation alloc]initWithCoordinate:coord title:titleStr];
        
        MyAnnotation*  annotObj =[[MyAnnotation alloc]initWithCoordinates:coord placeName:titleStr description:res.strAddr point:i];
        [self.mapView addAnnotation:annotObj];
        
        
        
        
        
        
        
//        MKPointAnnotation *Pin = [[MKPointAnnotation alloc]init];
//        Pin.coordinate = coord;
//        Pin.title = titleStr;
//        Pin.subtitle = res.strAddr;
//        [self.mapView addAnnotation:Pin];
        
        
    }
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    
    MKAnnotationView *annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"loc"];
    annotationView.canShowCallout = YES;
    
    
    UIButton *infoButton = [UIButton buttonWithType:UIButtonTypeInfoDark];
    [infoButton addTarget:self action:@selector(infoButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    MyAnnotation *ann=[[MyAnnotation alloc]init];
    ann=annotation;
    infoButton.tag=ann.pointid;
    annotationView.rightCalloutAccessoryView = infoButton;
    
    NSLog(@"DATA: %d",ann.pointid);
    
    return annotationView;
}


-(IBAction)infoButtonPressed:(id)sender
{
    NSLog(@"TAG: %d",[sender tag]);
    ResList *res = [self.arrRes objectAtIndex:[sender tag]];
//    [self.navigationController pushViewController:newView animated:YES];
    
    [self performSegueWithIdentifier:@"resdetailsvc" sender:res];
    
    
    
    
    
    
    
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    
    if([segue.identifier isEqualToString:@"resdetailsvc"]) {
        ResList *res = sender;
        NSLog(@"ID %@",res.strResId);
        ResdetailsVC *controller = (ResdetailsVC *)segue.destinationViewController;
        controller.resId=res.strResId;
        controller.rest=res;
        
    }
    
        
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


-(void)processCompleted:(NSInteger)btnTag{
    if(btnTag==100){
        [self performSegueWithIdentifier:@"seguesearch" sender:self];
    }else if(btnTag==300){
        [self performSegueWithIdentifier:@"segueMyDraw" sender:self];
    }
}

@end
